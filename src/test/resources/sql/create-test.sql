create user TESTFILEDB identified by pass;
alter session set current_schema=TESTFILEDB;
grant all privileges to TESTFILEDB;

create sequence "DOC_SEQ" minvalue 1 maxvalue 999999999999999999999999999 increment by 1 start with 1;
create table DOCUMENT ("ID" number(20,0) not null primary key, "NAME" NVARCHAR2(30) not null, "DATA" blob not null, "LOADED" date not null);