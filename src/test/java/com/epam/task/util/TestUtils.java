package com.epam.task.util;

import com.epam.task.domain.Document;
import com.epam.task.domain.DocumentMetadata;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Pavel_Mikhailiuk on 10/19/2016.
 */
public class TestUtils {

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static Document createDocument(String date) throws ParseException {
        return new Document(null, "testFile.txt", "test file content".getBytes(), stringToDate(date));
    }

    public static List<DocumentMetadata> createDocumentList() {
        DocumentMetadata documentMetadata1 = new DocumentMetadata(1L, "testFile1.txt");
        DocumentMetadata documentMetadata2 = new DocumentMetadata(2L, "testFile2.txt");
        DocumentMetadata documentMetadata3 = new DocumentMetadata(3L, "testFile3.txt");
        return Arrays.asList(documentMetadata1, documentMetadata2, documentMetadata3);
    }

    public static Date stringToDate(String dateString) throws ParseException {
        return new SimpleDateFormat(DATE_FORMAT).parse(dateString);
    }
}
