package com.epam.task.repository;

import com.epam.task.domain.Document;
import com.epam.task.domain.DocumentMetadata;
import com.epam.task.util.TestUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Pavel_Mikhailiuk on 10/19/2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DocumentRepositoryTest {

    @Autowired
    private DocumentRepository documentRepository;

    @Before
    public void preCleanup() {
        clean();
    }

    @After
    public void postCleanup() {
        clean();
    }

    @Test
    public void testSave() throws Exception {
        Document document = TestUtils.createDocument("2016-01-01");
        Document savedDocument = documentRepository.save(document);
        assertNotNull(savedDocument.getId());
        assertEquals(document, savedDocument);
    }

    @Test
    public void testFindOne() throws Exception {
        Document savedDocument = documentRepository.save(TestUtils.createDocument("2016-01-01"));
        Document foundDocument = documentRepository.findOne(savedDocument.getId());
        assertEquals(foundDocument, savedDocument);
    }

    @Test
    public void testSaveUpdate() throws Exception {
        Document savedDocument = documentRepository.save(TestUtils.createDocument("2016-01-01"));
        Long id = savedDocument.getId();
        Document foundDocument = documentRepository.findOne(id);
        assertEquals(foundDocument, savedDocument);
        savedDocument.setName("changedName");
        savedDocument.setData("new content".getBytes());
        documentRepository.save(savedDocument);
        foundDocument = documentRepository.findOne(id);
        assertEquals(savedDocument, foundDocument);
    }

    @Test
    public void testFindAllValid() throws Exception {
        documentRepository.save(TestUtils.createDocument("2016-01-01"));
        documentRepository.save(TestUtils.createDocument("2016-07-01"));
        documentRepository.save(TestUtils.createDocument("2016-10-01"));
        List<DocumentMetadata> foundDocuments = documentRepository.findAllValid(TestUtils.stringToDate("2016-06-01"));
        assertEquals(foundDocuments.size(), 2);
    }

    @Test
    public void testDelete() throws Exception {
        Document savedDocument = documentRepository.save(TestUtils.createDocument("2016-01-01"));
        Long id = savedDocument.getId();
        Document foundDocument = documentRepository.findOne(id);
        assertEquals(foundDocument, savedDocument);
        documentRepository.delete(id);
        foundDocument = documentRepository.findOne(id);
        assertNull(foundDocument);
    }

    @Test(timeout = 80)
    public void testSaveTimeout80() throws Exception {
        Document document = TestUtils.createDocument("2016-01-01");
        Document savedDocument = documentRepository.save(document);
        assertNotNull(savedDocument.getId());
        assertEquals(document, savedDocument);
    }

    @Test(timeout = 80)
    public void testFindOneTimeout80() throws Exception {
        Document savedDocument = documentRepository.save(TestUtils.createDocument("2016-01-01"));
        Document foundDocument = documentRepository.findOne(savedDocument.getId());
        assertEquals(foundDocument, savedDocument);
    }

    @Test(timeout = 100)
    public void testSaveUpdateTimeout100() throws Exception {
        Document savedDocument = documentRepository.save(TestUtils.createDocument("2016-01-01"));
        Long id = savedDocument.getId();
        Document foundDocument = documentRepository.findOne(id);
        assertEquals(foundDocument, savedDocument);
        savedDocument.setName("changedName");
        savedDocument.setData("new content".getBytes());
        documentRepository.save(savedDocument);
        foundDocument = documentRepository.findOne(id);
        assertEquals(savedDocument, foundDocument);
    }

    @Test(timeout = 100)
    public void testFindAllValidTimeout100() throws Exception {
        documentRepository.save(TestUtils.createDocument("2016-01-01"));
        documentRepository.save(TestUtils.createDocument("2016-07-01"));
        documentRepository.save(TestUtils.createDocument("2016-10-01"));
        List<DocumentMetadata> foundDocuments = documentRepository.findAllValid(TestUtils.stringToDate("2016-06-01"));
        assertEquals(foundDocuments.size(), 2);
    }

    @Test(timeout = 50)
    public void testDeleteTimeout50() throws Exception {
        Document savedDocument = documentRepository.save(TestUtils.createDocument("2016-01-01"));
        Long id = savedDocument.getId();
        Document foundDocument = documentRepository.findOne(id);
        assertEquals(foundDocument, savedDocument);
        documentRepository.delete(id);
        foundDocument = documentRepository.findOne(id);
        assertNull(foundDocument);
    }

    @Test(expected = RuntimeException.class)
    public void testSaveNullDocumentException() throws Exception {
        Document document = null;
        documentRepository.save(document);
    }

    @Test(expected = RuntimeException.class)
    public void testFindOneNullIdException() throws Exception {
        documentRepository.findOne(null);
    }

    @Test(expected = RuntimeException.class)
    public void testDeleteNullIdException() throws Exception {
        Long id = null;
        documentRepository.delete(id);
    }

    @Test
    public void testFindAllValidNullDate() throws Exception {
        List<DocumentMetadata> documentMetadataList = documentRepository.findAllValid(null);
        assertEquals(documentMetadataList.size(), 0);
    }

    private void clean() {
        documentRepository.deleteAll();
    }
}
