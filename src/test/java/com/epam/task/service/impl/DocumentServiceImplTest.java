package com.epam.task.service.impl;

import com.epam.task.domain.Document;
import com.epam.task.domain.DocumentMetadata;
import com.epam.task.exception.FileServiceException;
import com.epam.task.repository.DocumentRepository;
import com.epam.task.util.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Pavel_Mikhailiuk on 10/19/2016.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DocumentServiceImplTest {

    private static final String DOCUMENT_DATE = "2016-01-01";
    private static final Long DOCUMENT_ID = 1L;

    @Mock
    private DocumentRepository repository;

    @InjectMocks
    private DocumentServiceImpl documentService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSave() throws Exception {
        Document document = TestUtils.createDocument(DOCUMENT_DATE);
        Long expectedId = DOCUMENT_ID;
        document.setId(expectedId);
        when(repository.save(document)).thenReturn(document);
        Long actualId = documentService.save(document);
        verify(repository).save(document);
        assertEquals(actualId, expectedId);
    }

    @Test
    public void testDelete() throws Exception {
        Long documentId = DOCUMENT_ID;
        documentService.delete(documentId);
        verify(repository).delete(documentId);
    }

    @Test
    public void testUpdate() throws Exception {
        Document expectedDocument = TestUtils.createDocument(DOCUMENT_DATE);
        when(repository.save(expectedDocument)).thenReturn(expectedDocument);
        Document actualDocument = documentService.update(expectedDocument);
        verify(repository).save(expectedDocument);
        assertEquals(actualDocument, expectedDocument);

    }

    @Test
    public void testFind() throws Exception {
        Document expectedDocument = TestUtils.createDocument(DOCUMENT_DATE);
        Long id = DOCUMENT_ID;
        expectedDocument.setId(id);
        when(repository.findOne(id)).thenReturn(expectedDocument);
        Document actualDocument = documentService.find(id);
        verify(repository).findOne(id);
        assertEquals(actualDocument, expectedDocument);

    }

    @Test
    public void testFindAllValid() throws Exception {
        List<DocumentMetadata> expectedDocuments = TestUtils.createDocumentList();
        Date date = TestUtils.stringToDate(DOCUMENT_DATE);
        when(repository.findAllValid(date)).thenReturn(expectedDocuments);
        List<DocumentMetadata> actualDocuments = documentService.findAllValid(date);
        verify(repository).findAllValid(date);
        assertEquals(actualDocuments, expectedDocuments);

    }

    @Test(expected = FileServiceException.class)
    public void testFindAllValidException() throws Exception {
        Date date = TestUtils.stringToDate(DOCUMENT_DATE);
        when(repository.findAllValid(date)).thenThrow(new RuntimeException());
        documentService.findAllValid(date);
    }

    @Test(expected = FileServiceException.class)
    public void testFindException() throws Exception {
        Long id = DOCUMENT_ID;
        when(repository.findOne(id)).thenThrow(new RuntimeException());
        documentService.find(id);
    }

    @Test(expected = FileServiceException.class)
    public void testSaveException() throws Exception {
        Document document = TestUtils.createDocument(DOCUMENT_DATE);
        when(repository.save(document)).thenThrow(new RuntimeException());
        documentService.save(document);
    }

    @Test(expected = FileServiceException.class)
    public void testUpdateException() throws Exception {
        Document document = TestUtils.createDocument(DOCUMENT_DATE);
        when(repository.save(document)).thenThrow(new RuntimeException());
        documentService.update(document);
    }

    @Test(expected = FileServiceException.class)
    public void testDeleteException() throws Exception {
        Long id = DOCUMENT_ID;
        doThrow(new RuntimeException()).when(repository).delete(id);
        documentService.delete(id);
    }

    @Test(expected = FileServiceException.class)
    public void testDeleteNullIdException() throws Exception {
        documentService.delete(null);
    }

    @Test(expected = FileServiceException.class)
    public void testUpdateNullDocumentException() throws Exception {
        documentService.update(null);
    }

    @Test(expected = FileServiceException.class)
    public void testSaveNullDocumentException() throws Exception {
        documentService.save(null);
    }

    @Test(expected = FileServiceException.class)
    public void testFindNullIdException() throws Exception {
        documentService.find(null);
    }

    @Test(expected = FileServiceException.class)
    public void testFindAllValidNullDateException() throws Exception {
        documentService.findAllValid(null);
    }
}
