package com.epam.task.service;

import com.epam.task.domain.Document;
import com.epam.task.domain.DocumentMetadata;
import com.epam.task.exception.FileServiceException;

import java.util.Date;
import java.util.List;

public interface DocumentService extends CrudService<Document> {
    List<DocumentMetadata> findAllValid(Date expired) throws FileServiceException;
}
