package com.epam.task.repository;

import com.epam.task.domain.Document;
import com.epam.task.domain.DocumentMetadata;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface DocumentRepository extends CrudRepository<Document, Long> {

    @Query("select new com.epam.task.domain.DocumentMetadata(d.id, d.name) from Document d where d.loaded> :loaded")
    List<DocumentMetadata> findAllValid(@Param("loaded") Date loaded);
}
